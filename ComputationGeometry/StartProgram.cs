﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Customization;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Ribbon;
using Autodesk.AutoCAD.Runtime;
using Autodesk.Windows;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using ConvexHull;
using Autodesk.AutoCAD.Geometry;
using System.Windows.Interop;
using System.Drawing;
using BaseLibrary.ExtensionMethod;
using System.Threading;
using Acap = Autodesk.AutoCAD.ApplicationServices.Application;
using System.Windows;
using Application = Autodesk.AutoCAD.ApplicationServices.Application;

namespace ComputationGeometry
{
    public class StartProgram : IExtensionApplication
    {
        private string path = "";
        private Database db;
        private Document doc;
        private Editor ed;
        private static bool isAnimate;
        private static int delayTime = 100;

        public StartProgram() {
            doc = Application.DocumentManager.MdiActiveDocument;
            db = doc.Database;
            ed = doc.Editor;
            path = Path.GetDirectoryName(typeof(StartProgram).Assembly.Location);
        }


        [CommandMethod("GeneratePoint")]
        public void GeneratePoint()
        {

             db.GenerateManyPoint(100);

        }

        [CommandMethod("CreateConvexHull")]
        public void CreateConvexHull()
        {
            List<Point3d> points = db.GetAllPoints();
            db.CreateConvexHull(points,isAnimate,10);
        }

        [CommandMethod("CreateConvexHull1")]
        public void CreateConvexHull1()
        {
            List<Point3d> points = db.GetAllPoints();
            db.CreateConvexHull1(points, isAnimate, 10);
        }

        #region 初始化Ribbon
        public void Initialize()
        {
            AddRibbon();
        }

        public void Terminate()
        {

        }

        public void AddRibbon()
        {
            Autodesk.Windows.RibbonControl ribbonControl = RibbonServices.RibbonPaletteSet.RibbonControl;
            RibbonTab ribbonTab = new RibbonTab();
            ribbonTab.Title = "计算几何";
            ribbonTab.Id = Guid.NewGuid().ToString();
            ribbonControl.Tabs.Add(ribbonTab);
            ribbonTab.IsActive = true;

            #region 是否动画方式显示
            Autodesk.Windows.RibbonPanelSource animationPanelSource = AddRibbonPanelSource(ribbonTab, "动画");
            AddCheckBox(animationPanelSource, "是否动画");
            //AddTextBox(animationPanelSource, "动画延迟时间");
            #endregion

            #region 凸包Panel
            Autodesk.Windows.RibbonPanelSource convexHullPanelSource = AddRibbonPanelSource(ribbonTab, "凸包");
            AddRibbonBtn(convexHullPanelSource, "绘制随机点", "每次生成一百个随机点","GeneratePoint ");
            AddRibbonBtn(convexHullPanelSource, "生成凸包","算法1", "CreateConvexHull ");
            AddRibbonBtn(convexHullPanelSource, "生成凸包", "算法2", "CreateConvexHull1 ");
            #endregion
        }

        private Autodesk.Windows.RibbonPanelSource AddRibbonPanelSource(RibbonTab ribbonTab, string Title)
        {
            Autodesk.Windows.RibbonPanelSource ribbonPanelSource = new Autodesk.Windows.RibbonPanelSource();
            ribbonPanelSource.Title = Title;
            ribbonPanelSource.Id = Guid.NewGuid().ToString();
            RibbonPanel ribbonPanel = new RibbonPanel();
            ribbonPanel.Source = ribbonPanelSource;
            ribbonTab.Panels.Add(ribbonPanel);

            return ribbonPanelSource;
        }

        private void AddTextBox(Autodesk.Windows.RibbonPanelSource ribbonPanelSource,string labelName)
        {
            Autodesk.Windows.RibbonLabel ribbonLabel = new RibbonLabel();
            ribbonLabel.Size = RibbonItemSize.Standard;
            ribbonLabel.Text = labelName;
            ribbonPanelSource.Items.Add(ribbonLabel);

            Autodesk.Windows.RibbonTextBox ribbonTextBox = new RibbonTextBox();
            ribbonTextBox.Size = RibbonItemSize.Standard;
            ribbonTextBox.PropertyChanged += RibbonTextBox_PropertyChanged;
            ribbonPanelSource.Items.Add(ribbonTextBox);          
        }

        private void RibbonTextBox_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            Autodesk.Windows.RibbonTextBox ribbonTextBox = sender as Autodesk.Windows.RibbonTextBox;
            bool isTry = int.TryParse(ribbonTextBox.Text,out int result);
            if (isTry)
            {
                delayTime = result;
            }
            else
            {
                delayTime=100;
                //MessageBox.Show("请输入整数");
            }
        }

        private void AddCheckBox(Autodesk.Windows.RibbonPanelSource ribbonPanelSource,string text)
        {
            Autodesk.Windows.RibbonCheckBox checkBox = new RibbonCheckBox();
            checkBox.Text = text;
            checkBox.PropertyChanged += CheckBox_PropertyChanged;
            ribbonPanelSource.Items.Add(checkBox);
        }

        private void CheckBox_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            Autodesk.Windows.RibbonCheckBox checkBox = sender as Autodesk.Windows.RibbonCheckBox;
            if (checkBox.IsChecked == null)
            {
                isAnimate = false;
            }
            else if (checkBox.IsChecked == false)
            {
                isAnimate = false;
            }
            else
            {
                isAnimate = true; 
            }
        }

        private void AddRibbonBtn(Autodesk.Windows.RibbonPanelSource ribbonPanelSource, string Text,string ToolTip ,string CommandParameter)
        {
            Autodesk.Windows.RibbonButton ribbonButton = new Autodesk.Windows.RibbonButton();
            ribbonButton.UID = Guid.NewGuid().ToString();
            ribbonButton.Text = Text;
            ribbonButton.ShowText = true;
            ribbonButton.ShowImage = true;
            ribbonButton.ToolTip = ToolTip;
            ribbonButton.Size = RibbonItemSize.Large;
            ribbonButton.IsCheckable = true;
            ribbonButton.Orientation = System.Windows.Controls.Orientation.Vertical;
            ribbonButton.CommandParameter = CommandParameter;
            ribbonButton.CommandHandler = new AdskCommandHandler();
            SetRibbonBtnImage(ribbonButton);
            ribbonPanelSource.Items.Add(ribbonButton);
        }

        private void SetRibbonBtnImage(Autodesk.Windows.RibbonButton ribbonButton)
        {
            string imagePath = path + "\\Resources\\" + ribbonButton.Text + ".png";
            if (File.Exists(imagePath))
            {
                Bitmap bitmap = new Bitmap(imagePath);
                BitmapSource bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(bitmap.GetHbitmap(), IntPtr.Zero, System.Windows.Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                ribbonButton.LargeImage = bitmapSource;
                ribbonButton.Image = bitmapSource;
            }
        }
        #endregion

    }


}
