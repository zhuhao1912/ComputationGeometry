﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using BaseLibrary.ExtensionMethod;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ProgressBar;
using Acap = Autodesk.AutoCAD.ApplicationServices.Application;

namespace ConvexHull
{
    public static class Program
    {
        public static bool _isAnimate;
        public static int _delayTime;

        public static List<Point3d> GenerateManyPoint(this Database db, int num)
        {
            List<Point3d> points = new List<Point3d>();
            Random rdx = new Random();
            Random rdy = new Random();
            for (int i = 0; i < num; i++)
            {
                double x = rdx.NextDouble() * 200.0 - 100.0;
                double y = rdy.NextDouble() * 200.0 - 100.0;
                Point3d point = new Point3d(x, y, 0);
                points.Add(point);
                DBPoint dbPoint = new DBPoint(point);
                dbPoint.ColorIndex = rdx.Next(1,7);
                db.Pdmode = 35;
                db.Pdsize = 5;
                using (Transaction trans = db.TransactionManager.StartTransaction())
                {
                    db.Ex_AddToModelSpace(dbPoint);
                    trans.Commit();
                }
            }
            return points;
        }

        public static List<Point3d> GetAllPoints(this Database db)
        {
            List<Point3d> points = new List<Point3d>();
            Editor ed = db.Ex_GetEditor();
            PromptSelectionResult psr = ed.SelectAll();
            if (psr.Status == PromptStatus.OK)
            {
                 SelectionSet sSet = psr.Value;
                using (Transaction trans = db.TransactionManager.StartTransaction())
                {
                    foreach (ObjectId id in sSet.GetObjectIds())
                    {
                        DBPoint point = id.GetObject(OpenMode.ForRead) as DBPoint;
                        if (point != null)
                        {
                            points.Add(point.Position);
                        }
                    }
                    trans.Commit();
                }
            }
            return points;
        }

        public static List<Point3d> CreateConvexHull(this Database db,List<Point3d> points,bool isAnimate = false,int delayTime = 100)
        {
            _isAnimate = isAnimate; 
            _delayTime = delayTime;
            Point3d startPoitnt = CalRightPoint(points);
            List<Point3d> convexHullPoints = new List<Point3d>();
            convexHullPoints.Add(startPoitnt);
            CalAllConvexHullPoints(db,points, startPoitnt, startPoitnt, convexHullPoints, isAnimate, delayTime);
            db.PlotConvexHullPointLine(convexHullPoints);
            return convexHullPoints;
        }

        private static void PlotConvexHullPointLine(this Database db,List<Point3d> convexHullPoints)
        {
            using (Transaction trans = db.TransactionManager.StartTransaction())
            {
                for (int i = 0; i < convexHullPoints.Count; i++)
                {
                    if (i == convexHullPoints.Count - 1)
                    {
                        Line line = new Line(convexHullPoints.Last(),convexHullPoints.First());
                        db.Ex_AddToModelSpace(line);
                    }
                    else
                    {
                        Line line = new Line(convexHullPoints[i], convexHullPoints[i + 1]);
                        db.Ex_AddToModelSpace(line);
                    }
                }
                trans.Commit();
            }
        }

        private static Point3d CalRightPoint(List<Point3d> points)
        {
            var tempPoints = points.OrderBy(aa=>aa.X).ThenByDescending(aa=>aa.Y).ToList();
            return tempPoints.Last();
        }

        private static void CalAllConvexHullPoints(Database db,List<Point3d> points,Point3d basePoint,Point3d startPoint,List<Point3d> convexHullPoints, bool isAnimate, int delayTime)
        {
            Point3d q = new Point3d();
            for (int i = 0; i < points.Count; i++)
            {
                if (points[i] != basePoint)
                {
                    q = points[i];
                    break;
                }
            }
            for (int i = 0; i < points.Count; i++)
            {
                Point3d s = points[i];
                if (s.Y == q.Y || s.X == q.X) continue;
                if (s.Y == basePoint.Y || s.X == basePoint.X) continue;
                Entity ent1 = db.DrawLine(basePoint,q,1);
                Entity ent2 = db.DrawLine(basePoint,s,1);
                Entity ent3 = db.DrawLine(q, s, 1);
                double result = ToLeft(basePoint, q, s);
                if (result >= 0.0)
                {
                    q = s;
                }
                db.DeleteEnt(ent1);
                db.DeleteEnt(ent2);
                db.DeleteEnt(ent3);
            }
            if (q.X == startPoint.X || q.Y == startPoint.Y)
            {
                db.DrawLine(convexHullPoints.Last(), convexHullPoints.First(), 3);
                return;
            }
            else
            {
                convexHullPoints.Add(q);
                int count = convexHullPoints.Count;
                db.DrawLine(convexHullPoints[count - 1], convexHullPoints[count - 2],3);
                CalAllConvexHullPoints(db,points, q, startPoint, convexHullPoints, isAnimate, delayTime);
            }
        }

        private static double ToLeft(Point3d p,Point3d q,Point3d s)
        {
            return p.X * q.Y - p.Y * q.X + q.X * s.Y - q.Y *s.X + s.X * p.Y - s.Y * p.X; 
        }

        private static Entity DrawLine(this Database db,Point3d p1,Point3d p2,int colorIndex)
        {
            using (Transaction trans = db.TransactionManager.StartTransaction())
            {
                Line ent = new Line(p1, p2);
                ent.LinetypeScale = 1.5;
                ent.ColorIndex = colorIndex;
                db.Ex_AddToModelSpace(ent);
                if (_isAnimate)
                {
                    CadSystem.UpdateScreenEx(ent);
                    Thread.Sleep(_delayTime);
                }
                trans.Commit();
                return ent;
            }
        }

        private static void DeleteEnt(this Database db,Entity ent)
        {
            using (Transaction trans = db.TransactionManager.StartTransaction())
            {
                ent = trans.GetObject(ent.ObjectId,OpenMode.ForWrite) as Entity;
                if (ent != null)
                {
                    ent.Erase();
                    trans.Commit();
                }
            }
        }
    }

    public class CadSystem
    {
        public static void UpdateScreenEx(Entity ent = null)
        {
            ent?.Draw();//图元刷新
            ent?.RecordGraphicsModified(true);//图块刷新
            if (ent is Dimension dim)//标注刷新
                dim.RecomputeDimensionBlock(true);
            Acap.UpdateScreen();//和ed.UpdateScreen();//底层实现差不多
            //acad2014及以上要加,立即处理队列上面的消息
            System.Windows.Forms.Application.DoEvents();
        }
    }
}
