﻿using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using BaseLibrary.ExtensionMethod;
using BaseLibrary.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConvexHull
{
    public static class Algorithm2
    {
        public static bool _isAnimate;
        public static int _delayTime;
        public static Database _db;
        public static List<Entity> _entities = new List<Entity>();


        public static void CreateConvexHull1(this Database db, List<Point3d> points, bool isAnimate = true, int delayTime = 100)
        {
            DateTime dateTime = DateTime.Now;
            _isAnimate = isAnimate;
            _delayTime = delayTime;
            _db = db;
            PreSort(points,out Point3d basePoint,out List<Point3d> sortPoints);
            List<Point3d> convexPoints = new List<Point3d>();
            convexPoints.Add(basePoint);
            convexPoints.Add(sortPoints.First());

           _entities.Add(_db.DrawLine(convexPoints.First(), convexPoints.Last(),5));

            sortPoints.RemoveAt(0);
            TrackBack(sortPoints, convexPoints);
            db.PlotConvexHullPointLine(convexPoints);
            TimeSpan ts = DateTime.Now - dateTime;
            MessageBox.Show("耗时" + ts.TotalMilliseconds / 1000.0 + "秒");
        }

        private static void PlotConvexHullPointLine(this Database db, List<Point3d> convexHullPoints)
        {
            using (Transaction trans = db.TransactionManager.StartTransaction())
            {
                for (int i = 0; i < convexHullPoints.Count; i++)
                {
                    if (i == convexHullPoints.Count - 1)
                    {
                        Line line = new Line(convexHullPoints.Last(), convexHullPoints.First());
                        db.Ex_AddToModelSpace(line);
                    }
                    else
                    {
                        Line line = new Line(convexHullPoints[i], convexHullPoints[i + 1]);
                        db.Ex_AddToModelSpace(line);
                    }
                }
                trans.Commit();
            }
        }

        public static void TrackBack(List<Point3d> sortPoints,List<Point3d> convexPoints)
        {
            int count = convexPoints.Count;
            if (sortPoints.Count == 0)
            {
                return;
            }

            double toLeft = ToLeft(convexPoints[count - 2], convexPoints[count - 1], sortPoints.First());
            Entity ent = _db.DrawLine(convexPoints[count - 1],sortPoints.First(),6);
            _db.DeleteEnt(ent);

            if (toLeft > 0.0)
            {
                convexPoints.Add(sortPoints.First());
                _entities.Add(_db.DrawLine(convexPoints.Last(), convexPoints[convexPoints.Count - 2], 5));
                sortPoints.RemoveAt(0);
                TrackBack(sortPoints, convexPoints);
            }
            else
            {
                convexPoints.RemoveAt(count - 1);
                _db.DeleteEnt(_entities.Last());
                _entities.Remove(_entities.Last());
                TrackBack(sortPoints, convexPoints);
            }
        }

        /// <summary>
        /// 预排序
        /// </summary>
        /// <param name="points"></param>
        /// <param name="bottomLeftPoint"></param>
        /// <param name="sortPoints"></param>
        public static void PreSort(List<Point3d> points,out Point3d bottomLeftPoint,out List<Point3d> sortPoints)
        {

            sortPoints = points.OrderBy(aa => aa.X).ThenBy(bb => bb.Y).ToList();
            bottomLeftPoint = sortPoints.First();
            sortPoints.Remove(bottomLeftPoint);
            QuickSort(sortPoints,bottomLeftPoint,0,sortPoints.Count - 1);

            
            for (int i = 0; i < sortPoints.Count; i++)
            {
                using (Transaction trans = _db.TransactionManager.StartTransaction())
                {
                    Entity ent = _db.DrawLine(bottomLeftPoint, sortPoints[i], 1);
                    _db.DeleteEnt(ent);
                    trans.Commit();
                }
            }

        }

        /// <summary>
        /// 快速排序
        /// </summary>
        /// <param name="points"></param>
        /// <param name="basePoint"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        private static void QuickSort(List<Point3d> points,Point3d basePoint,int begin,int end)
        {
            if(begin < end)
            {
                int i = begin;
                int j = end;
                Point3d pivot = points[i];
                while(i < j)
                {
                    while (i < j && ToLeft(basePoint, pivot, points[j]) > 0.0)
                    {
                        j--;
                    }

                    if (i < j)
                    {
                        points[i] = points[j];
                        i++;
                    }

                    while (i < j && ToLeft(basePoint, pivot, points[i]) < 0.0)
                    {
                        i++;
                    }

                    if (i < j)
                    {
                        points[j] = points[i];
                        j--;
                    }
                }
                points[i] = pivot;
                QuickSort(points, basePoint, begin, i - 1);
                QuickSort(points, basePoint, i + 1, end);
            }
        }

        /// <summary>
        /// to-left测试
        /// </summary>
        /// <param name="p"></param>
        /// <param name="q"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        private static double ToLeft(Point3d p, Point3d q, Point3d s)
        {
            return p.X * q.Y - p.Y * q.X + q.X * s.Y - q.Y * s.X + s.X * p.Y - s.Y * p.X;
        }

        private static Entity DrawLine(this Database db, Point3d p1, Point3d p2, int colorIndex)
        {
            using (Transaction trans = db.TransactionManager.StartTransaction())
            {
                Line ent = new Line(p1, p2);
                ent.LinetypeScale = 1.5;
                ent.ColorIndex = colorIndex;
                db.Ex_AddToModelSpace(ent);
                if (_isAnimate)
                {
                    CadSystem.UpdateScreenEx(ent);
                    Thread.Sleep(_delayTime);
                }
                trans.Commit();
                return ent;
            }
        }

        private static void DeleteEnt(this Database db, Entity ent)
        {
            using (Transaction trans = db.TransactionManager.StartTransaction())
            {
                ent = trans.GetObject(ent.ObjectId, OpenMode.ForWrite) as Entity;
                if (ent != null)
                {
                    ent.Erase();
                    trans.Commit();
                }
            }
        }
    }
}
