﻿using Autodesk.AutoCAD.DatabaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acap = Autodesk.AutoCAD.ApplicationServices.Application;

namespace ConvexHull
{
    public class CadSystem
    {
        public static void UpdateScreenEx(Entity ent = null)
        {
            ent?.Draw();//图元刷新
            ent?.RecordGraphicsModified(true);//图块刷新
            if (ent is Dimension dim)//标注刷新
                dim.RecomputeDimensionBlock(true);
            Acap.UpdateScreen();//和ed.UpdateScreen();//底层实现差不多
            //acad2014及以上要加,立即处理队列上面的消息
            System.Windows.Forms.Application.DoEvents();
        }
    }
}
